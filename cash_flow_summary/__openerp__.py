{
    'name' : 'Cash Flow Summary',
    'version' : '1.0',
    'author' : 'Niks',
    'category' : 'Report',
    'description' : """
    
    """,
    'website': 'nothing',
    'images' : [],
    'depends' : ['base','account','report_xls'],
    'data': [
        "report_paperformat.xml",
        "report_generalledger1.xml",             
        "wizard/account_report_general_ledger1_view.xml",
    ],
    'installable': True,
    'auto_install': False,
}